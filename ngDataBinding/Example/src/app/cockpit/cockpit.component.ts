import { Component, OnInit, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html'
})
export class CockpitComponent implements OnInit {
  /* creación de las propiedades de clase que emitiran los eventos a través del EventEmitter */
  @Output() serverCreated = new EventEmitter<{
    serverName: string;
    serverContent: string;
  }>();
  @Output() blueprintCreated = new EventEmitter<{
    serverName: string;
    serverContent: string;
  }>();
  /* Al usar la referencia del html se comenta el atributo que guarda el nombre
  newServerName = '';
  newServerContent = ''; */

  /* La manera de accesar desde el typescript hacia una referencia dentro del template html del componente es a través del
  decorador ViewChild. Lo que contendrá el objeto una vez logueandolo en la consola es de tipo ElementRef*/
  @ViewChild('serverContentInput') serverContentInput: ElementRef;

  constructor() {}

  ngOnInit() {}

  /* Haciendo uso de las referencias locales se extrae el valor de un input */
  onAddServer(nameInput: HTMLInputElement) {
    // console.log(nameInput.value);
    /* para emitir información con el objeto creado en las propiedades de la clase */
    this.serverCreated.emit({
      serverName: nameInput.value,
      serverContent: this.serverContentInput.nativeElement.value
    });
  }

  onAddBlueprint(nameInput: HTMLInputElement) {
    this.blueprintCreated.emit({
      serverName: nameInput.value,
      serverContent: this.serverContentInput.nativeElement.value
    });
  }
}
