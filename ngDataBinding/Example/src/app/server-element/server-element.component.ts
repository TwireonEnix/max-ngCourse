import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnChanges,
  SimpleChanges,
  DoCheck,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ContentChild,
  AfterContentInit
} from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
  /* El cómo ng maneja la encapsulación  de las vistas */
  encapsulation: ViewEncapsulation.Emulated // None, Native
})
export class ServerElementComponent implements OnInit, OnChanges, DoCheck, AfterViewInit, AfterContentInit {
  /* Manera en la que se le pasa información del componente padre al componente hijo*/
  @Input() element: { type: string; name: string; content: string };
  @ViewChild('heading') heading: ElementRef;
  @ContentChild('contentParagraph') paragraph: ElementRef;

  constructor() {
    console.log('constructor called');
  }

  /* para ver los cambios que se están realizando, el método OnChanges es el único de los lifehooks que reciben parámetros */
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    console.log('onChanges called');
  }

  ngOnInit() {
    console.log('onInit called');
    console.log('Text Content: ' + this.heading.nativeElement.textContent);
    console.log('Paragraph Content: ' + this.paragraph.nativeElement.textContent);
  }

  ngDoCheck() {
    console.log('doCheck called');
  }

  ngAfterViewInit() {
    console.log('Text Content: ' + this.heading.nativeElement.textContent);
  }

  ngAfterContentInit() {
    console.log('Paragraph Content' + this.paragraph.nativeElement.textContent);
  }

}
