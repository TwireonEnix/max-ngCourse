import { ShoppingListService } from './../shopping-list.service';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Ingredient } from '../../shared/ingedient.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {
  @ViewChild('nameInput')
  ingredientName: ElementRef;
  @ViewChild('amountInput')
  ingredientAmount: ElementRef;

  constructor(private shoppingListService: ShoppingListService) {}

  ngOnInit(): void {}

  onAddingIngredient() {
    // this.shoppingListService.updateIngredients.emit({
    //   name: this.ingredientName.nativeElement.value,
    //   amount: +this.ingredientAmount.nativeElement.value
    // });
    this.shoppingListService.onIngredientAdded({
      name: this.ingredientName.nativeElement.value,
      amount: this.ingredientAmount.nativeElement.value
    });
  }
}
