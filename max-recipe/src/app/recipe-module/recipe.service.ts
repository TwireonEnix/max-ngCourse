import { ShoppingListService } from './shopping-list.service';
import { Ingredient } from './../shared/ingedient.model';
import { Injectable, EventEmitter } from '@angular/core';
import { Recipe } from './recipe.model';


@Injectable({providedIn: 'root'})

export class RecipeService {
  private recipes: Recipe[] = [
    {
      name: 'Schnitzel',
      description: 'This is simply a test',
      imagePath:
        'https://www.bbcgoodfood.com/sites/default/files/recipe-collections' +
        '/collection-image/2013/05/frying-pan-pizza-easy-recipe-collection.jpg',
      ingredients: [{name: 'Meat', amount: 1}, {name: 'French Fries', amount: 20}]
    },
    {
      name: 'Second test recipe',
      description: 'Second test for the app',
      imagePath:
      'https://images.media-allrecipes.com/userphotos/600x600/965017.jpg',
      ingredients: [{name: 'Buns', amount: 2}, {name: 'Meat', amount: 1}]
    }
  ];
  /* removido por observables
  recipeSelected = new EventEmitter<Recipe>(); */

  constructor(private shoppingListService: ShoppingListService) {}

  getRecipes() {
    /* al regresar un this.recipes, se regresa la referencial arreglo original por como funciona js, entonces si se
    modifica algo del arreglo fuera del servicio afectará al arreglo manejado por el servicio. Por lo tanto se debe
    retornar una copia del arreglo con el operador de spread (en el video usa this.recipes.splice() sin argumentos,
    pero en ecmascript 6 el spread clona el arreglo) */
    return [...this.recipes];
  }

  getSingleRecipe(id: number) {
    return this.recipes[id];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

}
