import { Ingredient } from '../shared/ingedient.model';

/* Model blueprint de cómo se verá un objeto js */

export interface Recipe {
  name: string;
  description: string;
  imagePath: string;
  ingredients: Ingredient[];
}
