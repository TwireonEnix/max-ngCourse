import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RecipeComponent } from './base/recipe.component';
import { RecipeDetailComponent } from './detail/recipe-detail.component';
import { RecipeItemComponent } from './item/item.component';
import { ShoppingEditComponent } from './shopping-edit/shopping-edit.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { RecipeListComponent } from './list/list.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DropdownDirective } from '../shared/dropdown.directive';
import { RecipeStartComponent } from './recipe-start/recipe-start.component';
import { RouterModule } from '@angular/router';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';

@NgModule({
  declarations: [
    RecipeComponent,
    RecipeDetailComponent,
    RecipeItemComponent,
    RecipeListComponent,
    RecipeStartComponent,
    ShoppingEditComponent,
    ShoppingListComponent,
    DropdownDirective,
    RecipeEditComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    // RouterModule
  ],
  exports: [
    RecipeComponent,
    ShoppingListComponent,
    DropdownDirective
  ]
})

export class RecipeModule {}
