import { RecipeService } from './../recipe.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class RecipeItemComponent implements OnInit {

  // @Input() recipe: {name: string, description: string, imagePath: string};
  @Input() recipe: Recipe;
  @Input() index: number;
  /* eliminar el output sustituido por un servicio */
  // @Output() recipeSelected = new EventEmitter<void>();

  constructor(private recipeService: RecipeService) { }

  ngOnInit(): void { }

  /*
  onDetailsDisplayed() {
    this.recipeService.recipeSelected.emit(this.recipe);
  }
  */

}
