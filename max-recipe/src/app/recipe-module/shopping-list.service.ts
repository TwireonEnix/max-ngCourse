import { Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingedient.model';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})

export class ShoppingListService {

  private ingredients: Ingredient[] = [
    {name: 'Apples', amount: 5},
    {name: 'Tomatoes', amount: 6},
  ];
  ingredientsChanged = new Subject<Ingredient[]>();

  getIngredients() {
    return [...this.ingredients];
  }

  onIngredientAdded(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.ingredientsChanged.next([...this.ingredients]);
  }

  addIngredients(ingredients: Ingredient[]) {
    /* Si se hace un push del arreglo, el arreglo se incluye dentro del arreglo como un solo elemento
    para concatenar el arreglo se usa el operador spread de ecma6 */
    this.ingredients.push(...ingredients);
    this.ingredientsChanged.next([...this.ingredients]);
  }

}
