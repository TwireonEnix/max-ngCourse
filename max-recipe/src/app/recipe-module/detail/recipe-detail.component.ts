import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  /*
  para recibir el id del recipe a través del router se quita el input
  @Input() */ recipe: Recipe;
  id: number;

  constructor(
    private recipeService: RecipeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((param: Params) => {
      this.id = +param['id'];
      this.recipe = this.recipeService.getSingleRecipe(this.id);
    });
  }

  onAddingToShList() {
    this.recipeService.addIngredientsToShoppingList(this.recipe.ingredients);
  }

  onEditingRecipe() {
    // this.router.navigate(['edit'], {relativeTo: this.route});
    /*
    alternativa:
    */
    this.router.navigate(['../', this.id, 'edit'], {relativeTo: this.route});

  }

  onDeletingRecipe() {

  }

}
