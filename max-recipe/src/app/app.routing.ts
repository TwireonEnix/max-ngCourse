import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipeComponent } from './recipe-module/base/recipe.component';
import { ShoppingListComponent } from './recipe-module/shopping-list/shopping-list.component';
import { RecipeStartComponent } from './recipe-module/recipe-start/recipe-start.component';
import { RecipeDetailComponent } from './recipe-module/detail/recipe-detail.component';
import { RecipeEditComponent } from './recipe-module/recipe-edit/recipe-edit.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'recipes', pathMatch: 'full' },
  /* Como el RecipeComponente es quien recibe los children, dentro de este componente se usará el
  router oulet */
  {
    path: 'recipes',
    component: RecipeComponent,
    children: [
      { path: '', component: RecipeStartComponent },
      { path: 'new', component: RecipeEditComponent },
      { path: ':id', component: RecipeDetailComponent },
      { path: ':id/edit', component: RecipeEditComponent },
    ]
  },
  { path: 'shopping-list', component: ShoppingListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
