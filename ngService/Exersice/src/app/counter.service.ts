import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})

export class CounterService {
  actToIn = 0;
  inToAct = 0;

  incrementActiveToInactive() {
    this.actToIn++;
    console.log('To Inact ' + this.actToIn);
  }

  incrementInactiveToActive() {
    this.inToAct++;
    console.log('To Act ' + this.inToAct);
  }

}
