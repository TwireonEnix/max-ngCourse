import { LoggingService } from './logging.service';
import { Injectable } from '@angular/core';
import { EventEmitter } from 'events';

/* decorador para que se pueda inyectar un servicio dentro de este servicios */
@Injectable({providedIn: 'root'})

export class AccountsService {

  accounts = [
    {
      name: 'Master Account',
      status: 'active'
    },
    {
      name: 'Testaccount',
      status: 'inactive'
    },
    {
      name: 'Hidden Account',
      status: 'unknown'
    }
  ];
  statusUpdated = new EventEmitter<string>();

  /* inyeccción de servicios dentro de otros servicios */
  constructor(private logger: LoggingService) {}

  addAccount(name: string, status: string) {
    this.accounts.push({name: name, status: status});
    this.logger.logStatusChange(status);
  }

  updateStatus(id: number, status: string) {
    this.accounts[id].status = status;
    this.logger.logStatusChange(status);
  }

}
