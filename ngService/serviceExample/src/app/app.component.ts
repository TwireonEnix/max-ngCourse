import { AccountsService } from './services/accounts.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  /* blueprint de un arreglo de objetos js */
  accounts: {name: string, status: string}[] = [];

  constructor(private accountService: AccountsService) {}

  ngOnInit(): void {
    /* vanilla js, lo que se asigna aquí es la referencia en memoria de donde se encuentra el arreglo */
    this.accounts = this.accountService.accounts;
  }

}
