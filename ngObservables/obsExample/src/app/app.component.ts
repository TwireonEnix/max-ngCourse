import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './user/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  user1Activated = false;
  user2Activated = false;
  userSubs: Subscription;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userSubs = this.userService.userActivated.subscribe((id: number) => {
      id === 1 ? this.user1Activated = true : this.user2Activated = false;
      id === 2 ? this.user2Activated = true : this.user1Activated = false;
    });
  }

  ngOnDestroy() {
    this.userSubs.unsubscribe();
  }
}
