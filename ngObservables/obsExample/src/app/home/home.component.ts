import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs';
import {map} from 'rxjs/operators';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  numberSubs: Subscription;
  mySub: Subscription;

  constructor() {}

  ngOnInit() {
    /* Hay muchas maneras de crear observables.
    Interval sirve para emitir datos cada x miliseguntos. Me fijé antes
    de los videos, que esto crea un memory leak, porque cada vez que la
    página hace reload, se crea un nuevo observable y corre
    indefinidamente, es buena práctica desuscribirse de cada uno
    de los observables que se invoquen cuando el componente se
    destruya */
    const myNumbers = Observable.interval(1000).pipe(map((data: number) => {
      return data * 2;
    }));
    this.numberSubs = myNumbers.subscribe((number: Number) => {
      console.log(number);
    });

    /* Creación de un observable desde scratch */
    const myObs = Observable.create((observer: Observer<string>) => {
      setTimeout(() => {
        /* next empuja el paquete de datos */
        observer.next('first package');
      }, 2000);
      setTimeout(() => {
        /* next empuja el paquete de datos */
        observer.next('second package');
      }, 4000);
      setTimeout(() => {
        /* next empuja el paquete de datos */
        // observer.error('this does not work');
        /* El complete le indica al observer, que es quien emite los datos
        para que sean recibidos por el suscriptor, que ya ha terminado la
        tarea, por lo tanto, incluso si hay emisiones de datos después de
        un .complete(), ya no son emitidos */
        observer.complete();
      }, 6000);
    });

    /* suscripcion al observable para recibir los datos emitidos en sus
    varias secciones, los next, el error, y el completed */
    this.mySub = myObs.subscribe(
      (data: string) => {
        console.log(data);
      },
      (error: string) => {
        console.log(error);
      },
      () => {
        console.log('completed');
    });
  }


  ngOnDestroy() {
    this.numberSubs.unsubscribe();
    this.mySub.unsubscribe();
  }

}
