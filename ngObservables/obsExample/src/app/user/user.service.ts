import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})

export class UserService {
  /* Subject es un observable y un observer al mismo tiempo, puede recibir
  y emitir datos */
  userActivated = new Subject();

}
