import { Observable } from 'rxjs';
import { CanComponentDeactivate } from './can-deactivate.guard';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ServersService } from '../servers.service';

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.css']
})
export class EditServerComponent implements OnInit, CanComponentDeactivate {
  server: { id: number; name: string; status: string };
  serverName = '';
  serverStatus = '';
  allowEdit = false;
  /* Para crear un guard que no deje que la página se abandone si no se han guardado los cambios */
  changesSaved = false;

  constructor(
    private serversService: ServersService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe((queryparams: Params) => {
      this.allowEdit = queryparams['allowEdit'] === '1' ? true : false;
    });
    this.route.fragment.subscribe();
    const id = this.route.snapshot.params['id'];
    this.server = this.serversService.getServer(+id);
    this.serverName = this.server.name;
    this.serverStatus = this.server.status;
  }

  onUpdateServer() {
    this.serversService.updateServer(this.server.id, {
      name: this.serverName,
      status: this.serverStatus
    });
    this.changesSaved = true;
    /* ruta para subir un nivel relativo a desde donde se llamó el componente, salir del componente una vez que se ha terminado la edición*/
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  /* método implementado por la interfaz del guard, el cuál será llamado para evaluar si es posible dejar la ruta o no en base a
  los valores actuales de los atributos del componente */
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.allowEdit) {
      return true;
    }
    /* En esta sección se revisa si algun valor de los inputs ha cambiado, en referencia con los primeros que se cargaron en el componente
    si hay cambios sin haber guardad con el método onUpdateServer regresa un dialog confirm del mismo navegador (que se puede manejar con un
      modal o dialog) para regresar el boolean */
    if (this.serverName !== this.server.name || this.serverStatus !== this.server.status) {
      return confirm('Do you want to discard the changes?');
    } else {
      return true;
    }
  }

}
