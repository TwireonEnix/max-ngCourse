import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';

/* al exportar un interfaz con un método se forza a la clase que implemente la interfaz a tener el método dentro
en su implementación */
export interface CanComponentDeactivate {
  /* tal como se definen objetos en una interfaz de un modelo, se pueden definir bluprints de funciones
  que en este caso nos dice que la clase debe implemetar un método llamado canDeactivate, el cual será una
  función que no recibe parámetros, y devuelve el observable, la promesa o un bool */
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

/* Un guard siempre tiene que ser un servicio, para poder implementarlo, la clase debe implementar la interfaz de
ng Router llamada canDeactivate que envolverá la interfaz declarada previamente, con esto se asegura que el
componente se conecta con el servicio */
export class CanDeactivateGuard
  implements CanDeactivate<CanComponentDeactivate> {

    /* Esta clase necesita el canDeactivate que será llamada por ng cuando se intente dejar la ruta, recibe un componente, que debe ser
    de tipo que incluya una interfaz de CanComponentDeactivate */
  canDeactivate(
    component: CanComponentDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    /* regresa una ejecución del método canDeactivate que está definida en el componente que se llama. */
    return component.canDeactivate();
  }

}
