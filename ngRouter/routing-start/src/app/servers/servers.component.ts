import { Component, OnInit } from '@angular/core';
import { ServersService } from './servers.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  servers: {id: number, name: string, status: string}[] = [];

  /* se utiliza aquí para utilizar la ruta relativa una vez que ng sepa en qué ruta se encuentra mediante
  el objeto ActivatedRoute */
  constructor(private serversService: ServersService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.servers = this.serversService.getServers();
  }

  onReload() {
    /* con routerLink las rutas relativas funcionan porque identifica en qué página la aplicación está actualmente,
    programáticamente no se sabe. Para utilizar rutas relativas se hace con opciones del parámetro de navigate
    obteniendo la información con la inyección del objeto ActivatedRoute. */
    this.router.navigate(['servers'], {relativeTo: this.route});
  }

}
