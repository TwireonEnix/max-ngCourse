import { Component, OnInit, OnDestroy } from '@angular/core';

import { ServersService } from '../servers.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit, OnDestroy {
  server: { id: number; name: string; status: string };
  paramsSub: Subscription;

  constructor(
    private serversService: ServersService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.paramsSub = this.route.params.subscribe((params: Params) => {
      /* Los parámetros se reciben como strings y getServer espera un number,
      adicionar el + para hacer la conversión */
      this.server = this.serversService.getServer(+params['id']);
    });
  }

  onEdit() {
    /* para no escribir la ruta completa se usará una ruta relativa, al fin y al cabo el componente ya tiene la información necesaria
    queryParamsHandling sirve para preservar o combinar (merge) los parámetros anteriores y no se pierdan en la navegación*/
    this.router.navigate(['edit'], {relativeTo: this.route, queryParamsHandling: 'preserve'});
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }

}
