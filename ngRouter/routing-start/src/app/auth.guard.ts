import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
  CanActivateChild
} from '@angular/router';
import { Observable } from 'rxjs';

/* Se usa el decorador inyectable para poder hacer inyección de servicios externos, sin embargo no se debería usar
la configuración de provided in, sólo para servicios core. En este caso el guard-servicio entrará en el arreglo de
providers en el módulo que lo contenga */
@Injectable()

export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private authService: AuthService, private router: Router) {}

  /* ng provee los datos que necesita al ejecutar la clase lo que puede regresar el método es un bool de manera síncrona o
  asíncrona, asíncrona si se usa un servicio para verificar algo en algún servidor, al utilizar esta guarda específica, se tarda
  poco menos de un segundo en hacerlo dado que se implementó un timeout en el método isAuthenticated()*/
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.iSAuthenticated().then((authenticated: boolean) => {
      if (authenticated) {
        return true;
      } else {
        this.router.navigate(['/']);
      }
    });
  }

  /* ahora se puede utilizar un hook diferente en las rutas. para evitar que las rutas hijas necesiten de una guarda siempre que sean
  declaradas */
  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActivate(route, state);
  }

}
