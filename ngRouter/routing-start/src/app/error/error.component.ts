import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';

@Component({
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})

export class ErrorComponent implements OnInit {

  errorMessage: string;

  /* para recibir los datos estáticos en el ruteo, es necesaria la información de la ruta activada */
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    /* para recibir el mensaje estático de data en la ruta  */
    // this.errorMessage = this.route.snapshot.data['message'];
    this.route.data.subscribe((data: Data) => {
      this.errorMessage = data['message'];
    });
  }

}
