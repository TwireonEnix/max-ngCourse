import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})

export class AuthService {
  loggedIn = false;

  iSAuthenticated() {
    console.log(this.loggedIn);
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(this.loggedIn);
      }, 100);
    });
  }

  login() {
    this.loggedIn = true;
    console.log(this.loggedIn);
  }

  logout() {
    this.loggedIn = false;
  }

}
