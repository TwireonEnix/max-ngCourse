import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  user: { id: number; name: string };
  paramsSubs: Subscription;
  /* para tener acceso a la ruta actualmente cargada en la aplicación */
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.user = {
      /* Al usar snapshot params, el componente no es reactivo a cambios porque el componente ya fue
      cargado con una información de inicialización. Si el componente ya está cargado no se podrán
      realizar cambios. Snapshot está bien para la primer inicialización. */
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name']
    };
    /* params es un observable para trabajar con una tarea asíncrona, sin embargo esta obsoleto, usar
    paramMap. */
    this.paramsSubs = this.route.params
      .subscribe((params: Params) => {
        this.user.id = params['id'];
        this.user.name = params['name'];
      });
  }

  ngOnDestroy() {
    /* ng teoréticamente destruye subscripciones de la memoria, pero es una buena práctica hacerlo manualmente
    Hay que guardar la suscripción en un atributo de clase.  */
    this.paramsSubs.unsubscribe();
  }

}
