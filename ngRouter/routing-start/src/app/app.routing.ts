import { ErrorComponent } from './error/error.component';
import { CanDeactivateGuard } from './servers/edit-server/can-deactivate.guard';
import { AuthGuard } from './auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { ServersComponent } from './servers/servers.component';
import { ServerComponent } from './servers/server/server.component';
import { EditServerComponent } from './servers/edit-server/edit-server.component';
import { NotFoundComponent } from './page-not-found/page-not-found.component';

const appRoutes: Routes = [
  /* cada ruta es un objeto js en este arreglo, que siguen un patrón específico */
  { path: '', component: HomeComponent, pathMatch: 'full' },
  {
    path: 'users',
    component: UsersComponent,
    children: [
      /* parámetros dentro de una ruta, el : indica que es una parte dinámica de la ruta, en este caso,
  todo lo demás despues de users/--- será interpretado como el id.*/
      { path: ':id/:name', component: UserComponent }
    ]
  },
  {
    path: 'servers',
    component: ServersComponent,
    /* los guards de canActivate se aplican a todos los child routes para seleccionar únicamente las rutas hijas con algún
    guard definido se hace a través de canActivateChild, esto hace que sea posible activar al padre, pero no a los hijos*/
    // canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    /* Los hijos se accesarán a través de /servers/algo, tendrán su único routerOutlet */
    children: [
      { path: ':id', component: ServerComponent },
      {
        path: ':id/edit',
        component: EditServerComponent,
        /* correrá el guard cuando se intente navegar fuera de esta ruta */
        canDeactivate: [CanDeactivateGuard]
      }
    ]
  },
  { path: 'not-found', component: NotFoundComponent },
  /* Hay manera de pasar datos estáticos al componente a través de la opción data como un objeto js con kvp's*/
  { path: 'error', component: ErrorComponent, data: {message: 'Page not found!'}},
  /* para hacer una redirección se especifica la ruta donde se va a redireccionar, y con un doble asterisco se hace una wildcard
  el orden es importante, el wildcard debe siempre estar hasta abajo, porque las rutas se parsean de arriba hacia abajo*/
  { path: '**', redirectTo: 'error' }
];

@NgModule({
  imports: [
    /* Agrega la funcionalidad del router de angular, el extra .forRoot() le dice a ng que son rutas para la raíz de la app*/
    RouterModule.forRoot(appRoutes)
  ],
  /* Para que sea accesible por otro módulo incluyendo este */
  exports: [RouterModule],
  /* para poder usar las guardas, que son servicios. Todos los servicios que no tengan el decorador en Injectable con la configuración
  providedIn: root deben ir en los proveedores. Los guards siempre se colocan en los providers !A menos que inyecten en su clase algún
  servicio */
  providers: [CanDeactivateGuard, AuthGuard]
})
export class AppRoutingModule {}
