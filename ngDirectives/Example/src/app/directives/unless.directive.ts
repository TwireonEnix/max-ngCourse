import { Directive, OnInit, Input, TemplateRef, ViewContainerRef } from '@angular/core';
@Directive({
  selector: '[appUnless]'
})

/* Directiva que hace lo contrario de ngIf, éste despliega un elemento del dom en caso de un valor false */
export class UnlessDirective implements OnInit {

  /* Input para hacer property binding y recibir el valor el nombre de la propiedad debe ser igual al selector
  para que sea bindeable desde fuera con *appUnless */
  @Input() set appUnless(cond: boolean) {
    cond ? this.vcRef.clear() : this.vcRef.createEmbeddedView(this.templateRef);
  }

  constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) {}

  ngOnInit() {}

}
