import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  /* selector de la directiva para uso, en brackets */
  selector: '[appBasicHighlight]',
  /* las directivas no tienen vista , ni templates*/
})

/* las directivas solo tienen los hooks de Init y Destroy*/
export class BasicHighlightDirective implements OnInit {

  /* Inyección: ng crea las inyecciones y las instancia para poder ser usadas en la clase */
  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    /* No es buena práctica este approach para aplicar el estilo ya que hay momentos en los que
    no existe el elemento en el dom. La solución más óptima es el renderer. */
    this.elementRef.nativeElement.style.backgroundColor = 'green';
  }

}
