import { Directive, OnInit, Renderer2, ElementRef, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})

export class BetterHighlightDirective implements OnInit {

  /* Para que no tenga valores fijos es posible hacer property binding con el decorador input para modificar el valor
  de la directiva dinámicamente. Cuando un input utiliza valores por default, éstos pueden ser sobreescritos por fuera */
  @Input() defaultColor = 'transparent';
  /* Es posible poner un alias llamado igual que la directiva para usar únicamente la directiva y su valor, sin embargo
  tsLint lintea este comportamiento dado que: 'Two names for the same property (one private, one public) is inherently confusing.' */
  // tslint:disable-next-line:no-input-rename
  @Input('appBetterHighlight') highlightColor = 'blue';

  /* Otra manera de cambiar el estilo rápidamente es con el decorador HostBinding, se le pasa
  como argumento la propiedad del elemento al que se va a enlazar */
  @HostBinding('style.backgroundColor') backgroundColor: string;

  /* Renderer es la mejor herramienta para crear directivas */
  constructor(private elRef: ElementRef , private renderer: Renderer2) {}

  ngOnInit() {
    /* uso del renderer para trabajar con el dom, el renderer tiene varios métodos, en este caso se usa el setStyle
    función que recibe 4 argumentos, el elemento con la referencia de elementRef, el estilo, y el valor del estilo, el
    cuarto parámetro es opcional */
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');

    this.backgroundColor = this.defaultColor;
  }

  /* es posible configurar la directiva para activarla sólo en el caso de que ciertos eventos la triggereen. El decorador
  HostListener toma el nombre de un evento que ya exista en el elemento del dom donde se colocará la directiva. El decorador
  es una manera conveniente de escuchar los eventos y lanzar alguna acción en base a ellos, los eventos son los normales
  de los elementos dom (click, hover, etc). */
  @HostListener('mouseenter') mouseOver(eventData: Event) {
     /* Manera de cambiarlo con el renderer */
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'blue');

    /* Manera de cambiar el estilo utilizando el HostBinding declarado como atributo de clase y el decorador que apunta al estilo
    que se va a modificar (o atributo del elemento) */
    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave') mouseLeave(eventData: Event) {
    // this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');
    this.backgroundColor = this.defaultColor;
  }

}

/* el renderer es un mejor acercamiento a las directivas de atributos dado que angular no solamente corre manipulando el
dom o en el browser, tambien funciona con service workers, objetos que no tienen acceso al dom. Entonces, en general es mejor
utilizar el renderer para manipulación del dom. */
