import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  genders = ['male', 'female'];
  signUpForm: FormGroup;
  forbiddenUserNames = ['Chris', 'Anna'];

  constructor() {}

  ngOnInit() {
    this.signUpForm = new FormGroup({
      /** Agrupar FormGroup */
      userData: new FormGroup({
        userName: new FormControl(null, [
          Validators.required,
          /** Al usar el this dentro de la función que valida el scope del this cambia hacia este objeto que está
           * llamando la función. el método bind es un trick que hace override a ese comportamiento haciendo que
           * el scope del this dentro del validador sea el de la clase del componente
           */
          this.forbiddenNames.bind(this)
        ]),
        email: new FormControl(
          null,
          [Validators.required, Validators.email],
          /** La referencia a la funcion validadora async no va en el mismo bloque que las que son normales */
          this.forbiddenEmails
        )
      }),
      gender: new FormControl('male'),
      /** Form Array para un número dinámico de controles dependiendo del usuario */
      hobbies: new FormArray([])
    });
    /** Observables a los que se les puede hacer una suscripción: valueChanges, statusChanges */

    this.signUpForm.valueChanges.subscribe(value => {
      console.log(value);
    });
    this.signUpForm.statusChanges.subscribe(status => {
      console.log(status);
    });
    /** SetValue */
    this.signUpForm.setValue({
      userData: {
        userName: 'Dario',
        email: 'admin@admin.com'
      },
      gender: 'male',
      hobbies: []
    });
  }

  onSubmit() {
    console.log(this.signUpForm);
    this.signUpForm.reset();
  }

  onAddHobby() {
    /** Se obtiene la referencia del arreglo dentro del group para poder agregar controles al arreglo. */
    (<FormArray>this.signUpForm.get('hobbies')).push(
      new FormControl(null, Validators.required)
    );
  }

  /** Creación de un validador costumizado, funcion que se ejecuta automáticamente al modificar los valores del
   * formControl
   */

  forbiddenNames(control: FormControl): { [s: string]: boolean } {
    if (this.forbiddenUserNames.indexOf(control.value) !== -1) {
      return { nameIsForbidden: true };
    }
    return null;
  }

  /** Validador costumizado asíncrono */

  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test@test.com') {
          resolve({ emailIsForbidden: true });
        } else {
          resolve(null);
        }
      }, 1500);
    });
  }
}
