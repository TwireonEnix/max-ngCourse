import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f')
  signUpForm: NgForm;

  suggestUserName() {
    const suggestedName = 'Superuser';
  }

  /** Se le pasa un parámetro utilizando referencia local en la form, el cuál tiene un tipo, HTMLFormElement
   * cuando la referencia se pasa tal cual, siendo un objeto html, pero parseando desde el template con
   * #f="ngForm" se convierte en un objeto javascript
   */
  // onSubmit(f: NgForm) {
  //   console.log(f);
  // }

  /** Usando viewChild para obtener la form*/

  onSubmit() {
    console.log(this.signUpForm);
  }
}
